# Workshop Clean Code
Ein Taxiunternehmen möchte eine App entwickeln, bei der die User im Voraus erfahren können, wie viel sie zahlen werden.
Hierfür ist es notwendig, einen Algorithmus zu schreiben, der die Fahrten im Voraus berechnen kann.
Es sind verschiedene Faktoren dafür relevant, darunter Kilometerstand des Fahrzeugs, die Fahrzeit oder auch die Art des Fahrzeugs.

## Berechnungen
Kosten = Grundpreis + Kosten pro Minute * Fahrzeit (in min)

### Kosten pro Minute werden reduziert je nach Kilometerstand des Fahrzeugs
Wenn der Kilometerstand > X ist, werden Y% vom Endbetrag abgezogen.
* Kilometerstand: 25.000 -> 5%
* Kilometerstand: 50.000 -> 10% 
* Kilometerstand: 100.000 -> 15% 
* Kilometerstand: 200.000 -> 20% 
* Kilometerstand: 500.000 -> 25% 

### Der Preis pro Minute wird reduziert, je nach Fahrzeit
Wenn die Fahrzeit X übersteigt, werden Y% vom Preis pro Minute abgezogen
* Fahrzeit: 30 -> 5%
* Fahrzeit: 60 -> 10%
* Fahrzeit: 90 -> 15%
* Fahrzeit: 120 -> 20%
* Fahrzeit: 150 -> 25%

### Je nach Fahrzeugtyp gibt es einen unterschiedlichen Aufschlag auf den Grundpreis.
Das Unternehmen möchte die Kunden stark für Hybrid- und Elektrofahrzeuge motivieren.
Wenn das Fahrzeug vom Typ X ist, bekommt es einen Kostenaufschlag Y auf den Grundpreis.
* Elektrisch: €0
* Hybrid: €5
* Benzin: €15
* Diesel: €20

### Bewertungen der Kunden
In der Zukunft sollen die FahrerInnen ihre KundInnen bewerten können.
Hierfür gibt es folgende Berechnungen:
* Sterne > 4.5 & Anzahl der Fahrten > 15: Sofern ein Hybrid- oder Elektroauto gebucht wurde, ist die Fahrt gratis und die Anzahl der Fahrten wird zurückgesetzt.
* Sterne < 2: Sofern ein Benzin- oder Dieselfahrzeug gebucht wurde, wird der Grundpreis verdoppelt.
* Sterne <= 1: Sofern ein Elektro- oder Hybridfahrzeug gebucht wurde, wird der Preis pro Minute verdoppelt, bei Benzin- oder Dieselfahrzeugen verdreifacht.