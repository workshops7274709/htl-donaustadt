﻿using System;

namespace dotnetcore
{
    class Program
    {
        public static void Check(string x)
        {
            bool y = false;
            for (int i = 0; i < Int32.Parse(x); i = 1 + i)
            {
                if (i == 0) continue;
                int a = Int32.Parse(x) % i;
                if (i != 0 && a == 0 && i != 1)
                {
                    y = true;
                    continue;
                }
            }
            Console.WriteLine(y ? "no" : "yes");
        }

        public static void CheckIfPrime(int number)
        {
            for (int i = 2; i <= number / 2; i++)
            {
                if (number % i == 0)
                {
                    Console.WriteLine($"{number} is not a prime number");
                    return;
                }
            }

            Console.WriteLine($"{number} is a prime number");
        }
    }
}
