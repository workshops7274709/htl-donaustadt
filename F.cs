﻿using System;

namespace dotnetcore
{
    class F
    {
        public string N { get; set; }
        public int Km { get; set; }
        public FT Ft { get; set; }
        public double Kpm { get; set; }
        public double Gp { get; set; }

        public enum FT
        {
            B,
            D,
            H,
            E
        }

        public F(string n, int km, FT ft, double kpm, double gp)
        {
            N = n;
            Km = km;
            Ft = ft;
            Kpm = kpm;
            Gp = gp;
        }
    }

    class K
    {
        public string N { get; set; }
        public double Rate { get; set; }
        public int Count { get; set; }

        public K(string n, double rate, int count)
        {
            N = n;
            Rate = rate;
            Count = count;
        }
    }

    public class Data
    {
        public static void Main(string[] args)
        {
            K k1 = new K("Martin T.", 4.9, 12);
            K k2 = new K("Thomas S.", 4.2, 60);
            K k3 = new K("Andrea M.", 3.7, 0);
            K k4 = new K("Herbert F.", 3.3, 90);
            K k5 = new K("Gabriele S.", 2.8, 5);
            K k6 = new K("Helmut S.", 2.3, 24);
            K k7 = new K("Reinhard L.", 1.7, 45);
            K k8 = new K("Bernhard H.", 1.0, 3);

            F f1 = new F("Mercedes Sprinter TDI", 30000, F.FT.D, 1.2, 20);
            F f2 = new F("Hyundai Ionic E", 5000, F.FT.E, 1.1, 10);
            F f3 = new F("Trabant 601", 652000, F.FT.D, 2.0, 20);
            F f4 = new F("Nissan GTR R35", 150, F.FT.B, 3.8, 30);
            F f5 = new F("Mercedes W14", 4000, F.FT.H, 10.9, 60);
            F f6 = new F("Aston Martin DB5", 500000, F.FT.B, 7.0, 30);
            F f7 = new F("Peugeot Quartz", 50, F.FT.H, 1.1, 40);
            F f8 = new F("Hyundai Ionic E", 23000, F.FT.E, 2.1, 20);

            double s1 = Calc(31, k1, f1);
            Console.WriteLine($"{string.Format("{0:F2}", s1)} should be 71,57");

            double s2 = Calc(16, k2, f2);
            Console.WriteLine($"{string.Format("{0:F2}", s2)} should be 27,60");

            double s3 = Calc(17, k3, f3);
            Console.WriteLine($"{string.Format("{0:F2}", s3)} should be 55,50");

            double s4 = Calc(21, k4, f4);
            Console.WriteLine($"{string.Format("{0:F2}", s4)} should be 124,80");

            double s5 = Calc(91, k5, f5);
            Console.WriteLine($"{string.Format("{0:F2}", s5)} should be 785,86");

            double s6 = Calc(220, k6, f6);
            Console.WriteLine($"{string.Format("{0:F2}", s6)} should be 573,21");

            double s7 = Calc(330, k7, f7);
            Console.WriteLine($"{string.Format("{0:F2}", s7)} should be 203,29");

            double s8 = Calc(10, k8, f8);
            Console.WriteLine($"{string.Format("{0:F2}", s8)} should be 62,00");      
        }

        private static double Calc(int fm, K k, F t)
        {
            double gp = t.Gp;
            double kpm = t.Kpm;

            if (k.Rate > 4.5 && k.Count > 15)
            {
                k.Count = 0;
                return 0;
            }
            else if (k.Rate <= 1) 
            {
                if (t.Ft == F.FT.E)
                {
                    kpm = kpm * 2;
                }
                if (t.Ft == F.FT.H)
                {
                    kpm = kpm * 2;
                }
                if (t.Ft == F.FT.B)
                {
                    kpm = kpm * 3;
                }
                if (t.Ft == F.FT.D)
                {
                    kpm = kpm * 3;
                }
            }
            else if (k.Rate < 2)
            {
                if (t.Ft == F.FT.B)
                {
                    gp = gp * 2;
                }
                if (t.Ft == F.FT.D)
                {
                    gp = gp * 2;
                }
            }

            if (fm > 150)
            {
                kpm = kpm * 75 / 100;
            }
            if (fm > 120)
            {
                kpm = kpm * 80 / 100;
            }
            if (fm > 90)
            {
                kpm = kpm * 85 / 100;
            }
            if (fm > 60)
            {
                kpm = kpm * 90 / 100;
            }
            if (fm > 30)
            {
                kpm = kpm * 95 / 100;
            } 

            if (t.Ft == F.FT.H)
            {
                gp = gp + 5;
            }
            if (t.Ft == F.FT.B)
            {
                gp = gp + 15;
            }
            if (t.Ft == F.FT.D)
            {
                gp = gp + 20;
            }

            double sum = gp + (fm * kpm);

            if (t.Km > 500000)
            {
                sum = sum * 75 / 100;
            } 
            else if (t.Km > 200000)
            {
                sum = sum * 80 / 100;
            } 
            else if (t.Km > 100000)
            {
                sum = sum * 85 / 100;
            }
            else if (t.Km > 200000)
            {
                sum = sum * 80 / 100;
            }
            else if (t.Km > 50000)
            {
                sum = sum * 90 / 100;
            }
            else if (t.Km > 25000)
            {
                sum = sum * 95 / 100;
            }

            k.Count += 1;

            return sum;
        }
    }
}
